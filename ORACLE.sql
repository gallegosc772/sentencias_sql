/*==============================================================*/
/* CREACION DE USUARIOS Y ROLES                                 */
/*==============================================================*/


alter session set "_Oracle_SCRIPT"=true; 

CREATE USER recepcionista IDENTIFIED BY 123456;

GRANT CONNECT to recepcionista;

CREATE ROLE ControlInscripcion;

GRANT SELECT ANY TABLE,INSERT ANY TABLE,UPDATE ANY TABLE,DELETE ANY TABLE TO ControlInscripcion;

GRANT ControlInscripcion TO recepcionista;
