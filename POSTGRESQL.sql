/*==============================================================*/
/* CREACION DE USUARIOS Y ROLES                                 */
/*==============================================================*/

CREATE ROLE ControlInscripcion;

GRANT SELECT,INSERT,UPDATE,DELETE on cliente,entrenador TO ControlInscripcion;

CREATE USER Recepcionista IN ROLE ControlInscripcion PASSWORD '123456';