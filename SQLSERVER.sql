
/*==============================================================*/
/* CREACION DE USUARIOS Y ROLES                                 */
/*==============================================================*/


create login recepcion with password=‘123456’;

create user recepcionista for login recepcion;

create role rol_controlInscripcion;

alter role rol_controlInscripcion add member[recepcionista];

Grant select, inset, update, delete to rol_controlInscripcion;